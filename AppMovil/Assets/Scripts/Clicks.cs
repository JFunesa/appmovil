using m08m17;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using static Cercle;

public class Clicks : MonoBehaviour
{
    
    private TextMeshProUGUI textMeshPro;
    [SerializeField]
    private ScriptableInt clicks;

    void Start()
    {
        textMeshPro = GetComponent<TextMeshProUGUI>();
        mostrar();
    }

    public void mostrar()
    {
        textMeshPro.text = clicks.valor + "";
    }
}
