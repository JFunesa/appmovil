using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BarraVida : MonoBehaviour
{
    private Image barraComida;
    [SerializeField]
    private ScriptableFloat comidaMax;

    [SerializeField]
    private ScriptableFloat comidaActual;



    void Start()
    {
        barraComida = GetComponent<Image>();
        mostrar();
    }

    public void mostrar()
    {
        barraComida.fillAmount = comidaActual.valor / comidaMax.valor;
    }
}
