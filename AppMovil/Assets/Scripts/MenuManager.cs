using BarraDeVida;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    public void GoPet()
    {
        GameManager.Instance.ChangeScene(GameManager.PetScene);
    }

    public void GoClicker()
    {
        GameManager.Instance.ChangeScene(GameManager.ClickerScene);
    }
}
