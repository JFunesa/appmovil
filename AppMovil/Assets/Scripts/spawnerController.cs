using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

public class spawnerController : MonoBehaviour
{
    [SerializeField] 
    private ScriptableSpawns[] spawnObjecte;

    void Start()
    {
        StopAllCoroutines();
        StartCoroutine(spawn());
    }
    IEnumerator spawn()
    {
        while (true)
        {
            int num = Random.Range(0, spawnObjecte.Length);
           GameObject objecte = Instantiate(spawnObjecte[num].objecte);
            objecte.transform.position = new Vector2(Random.Range(-2.5f, 2.5f), 6);
            objecte.GetComponent<Rigidbody2D>().velocity = new Vector2(spawnObjecte[num].speedX, -spawnObjecte[num].speedY - Random.Range(0, 2));
            yield return new WaitForSeconds(3.33f);
        }
        
    }
}
