using m08m17;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BarraDeVida
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager m_Instance;
        public static GameManager Instance => m_Instance;

        public const string PetScene = "Pet";
        public const string ClickerScene = "Clicker";

        [SerializeField]
        private ScriptableFloat comidaMax;

        [SerializeField]
        private ScriptableFloat comidaActual;

        [SerializeField]
        private ScriptableInt clicks;

        [SerializeField]
        private ScriptableInt cost;

        [SerializeField]
        private GameEvent m_comida_Event;


        private void Awake()
        {

            if (m_Instance == null)
            {
                m_Instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
            SceneManager.sceneLoaded += OnSceneLoaded;

            ChangeScene(ClickerScene);
            StartCoroutine(Baixar());
        }

        //Es crida al carregar una escena
        void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            Debug.Log("GameManager - OnSceneLoaded: " + scene.name);
        }

        public void ChangeScene(string scene)
        {
            SceneManager.LoadScene(scene);
        }

        IEnumerator Baixar()
        {
            while (true)
            {
                if (comidaActual.valor > 100)
                {
                    comidaActual.valor = 100;
                    yield return new WaitForSeconds(1.33f);
                }
                else if (comidaActual.valor > 0)
                {
                    comidaActual.valor -= 1 + Random.Range(0.1f, 2.0f);
                    yield return new WaitForSeconds(1.33f);
                    m_comida_Event?.Raise();
                } 
                else
                {
                    comidaActual.valor = 0;
                    yield return new WaitForSeconds(1.33f);
                }
                
            }

        }
    }
}
