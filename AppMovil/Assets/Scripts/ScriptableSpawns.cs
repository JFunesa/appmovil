using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableSpawns", menuName = "ScriptableObjects/ScriptableSpawns")]
public class ScriptableSpawns : ScriptableObject
{
    public int speedX;
    public int speedY;
    public GameObject objecte;
}
