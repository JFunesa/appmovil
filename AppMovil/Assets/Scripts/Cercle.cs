using m08m17;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.Utilities;

public class Cercle : MonoBehaviour
{
    [SerializeField]
    public float scale;

    [SerializeField]
    private GameEvent m_Event;

    [SerializeField]
    private ScriptableInt clicks;

    [SerializeField]
    private ScriptableFloat comida;




    // Update is called once per frame
    void Update()
    {

    }

    private void OnMouseDown()
    {
        //Botton que cliquee esto manin
        StopAllCoroutines();
        StartCoroutine(granpetit());
        clicks.valor += sumarClicks();
        m_Event?.Raise();
    }

    IEnumerator granpetit()
    {
        for (int i = 0; i < 5; i++)
        {
            transform.localScale = new Vector3(scale - i / 10f, scale - i / 10f, 1);
            yield return new WaitForSeconds(0.05f);
        }
        for (int i = 0; i < 5; i++)
        {
            transform.localScale = new Vector3((scale-0.4f) + i / 10f, (scale - 0.4f) + i / 10f, 1);
            yield return new WaitForSeconds(0.03f);
        }

    }
    public int sumarClicks()
    {
        int n = 1;
        if (comida.valor <= 25)
        {
            n = 1;
        }
        else if (comida.valor <= 50)
        {
            n = 2;
        }
        else if (comida.valor <= 75)
        {
            n = 3;
        }
        else if (comida.valor > 75)
        {
            n = 4;
        }
        return n;
    }
}
