﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableFloat", menuName = "ScriptableObjects/ScriptableFloat")]
public class ScriptableFloat : ScriptableObject
{
    public float valor;
}
