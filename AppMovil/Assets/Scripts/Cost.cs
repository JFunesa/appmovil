using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Cost : MonoBehaviour
{
    private TextMeshProUGUI textMeshPro;
    [SerializeField]
    private ScriptableInt cost;

    void Start()
    {
        textMeshPro = GetComponent<TextMeshProUGUI>();
        mostrar();
    }

    public void mostrar()
    {
        textMeshPro.text = cost.valor + "";
    }
}
