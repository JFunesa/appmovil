using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableInt", menuName = "ScriptableObjects/ScriptableInt")]
public class ScriptableInt : ScriptableObject
{
    public int valor;
}
