using m08m17;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PetMoviment : MonoBehaviour
{
    

    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;

    private SpriteRenderer m_SpriteRenderer;

    [SerializeField]
    private float m_MovementSpeed = 3f;

    [SerializeField]
    private ScriptableInt clicks;

    [SerializeField]
    private ScriptableInt cost;

    [SerializeField]
    private ScriptableFloat comida;

    [SerializeField]
    private GameEvent m_clicks_Event;

    [SerializeField]
    private GameEvent m_cost_Event;

    [SerializeField]
    private GameEvent m_comida_Event;

    public Animator animator;


    void Awake()
    {
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        m_Input = Instantiate(m_InputAsset);
        m_Input.FindActionMap("Default").FindAction("MenjarButton").performed += ActionPerformed;
        m_Input.FindActionMap("Default").FindAction("MenjarButton").performed += ButtonAction;
        m_MovementAction = m_Input.FindActionMap("Default").FindAction("Moviment");
        m_Input.FindActionMap("Default").Enable();
    }

    private void ButtonAction(InputAction.CallbackContext context)
    {
        if (clicks.valor >= cost.valor)
        {
            clicks.valor -= cost.valor;
            GameObject novacomida = ComidaPool.instance.GetPooledComida();
            if (novacomida != null)
            {
                novacomida.GetComponent<Transform>().position = new Vector2(Random.Range(-2.5f, 2.5f), Random.Range(-3f, 3f));
                novacomida.SetActive(true);
            }
            cost.valor += 10;
            m_clicks_Event?.Raise();
            m_cost_Event?.Raise();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Comida")
        {
            comida.valor += 10 + Random.Range(0.5f, 8f);
            m_comida_Event?.Raise();
            collision.gameObject.SetActive(false);
            
        }
        if (collision.transform.tag == "ComidaSpawner")
        {
            comida.valor += 3 + Random.Range(0.25f, 2f);
            m_comida_Event?.Raise();
            collision.gameObject.SetActive(false);
            Destroy(collision.gameObject);

        }
        if (collision.transform.tag == "Dany")
        {
            comida.valor -= 3 + Random.Range(0.5f, 3.0f);
            m_comida_Event?.Raise();
            collision.gameObject.SetActive(false);
            Destroy(collision.gameObject);
        }
        if (collision.transform.tag == "ParedEsquerra" && GameObject.FindGameObjectsWithTag("Player").Length < 2)
        {
            GameObject PetForaCamara = Instantiate(this.gameObject);
            PetForaCamara.GetComponent<Transform>().position = new Vector2(3.6f, this.transform.position.y);
            PetForaCamara.name = this.transform.name;
        }
        if (collision.transform.tag == "ParedDereta" && GameObject.FindGameObjectsWithTag("Player").Length < 2)
        {
            GameObject PetForaCamara = Instantiate(this.gameObject);
            PetForaCamara.GetComponent<Transform>().position = new Vector2(-3.65f, this.transform.position.y);
            PetForaCamara.name = this.transform.name;
            
        }
        if (collision.transform.tag == "ParedBorrar")
        {
           Destroy(this.gameObject);
        }
    }

    void Update()
    {
        
        if (this.GetComponent<Rigidbody2D>().velocity.x < 0)
        {
            this.GetComponent<SpriteRenderer>().flipX = true;
            animator.SetFloat("speed", Mathf.Abs(this.GetComponent<Rigidbody2D>().velocity.x));
        }
        else if(this.GetComponent<Rigidbody2D>().velocity.x > 0)
        {
            this.GetComponent<SpriteRenderer>().flipX = false;
            animator.SetFloat("speed", Mathf.Abs(this.GetComponent<Rigidbody2D>().velocity.x));
        }
        else if (Mathf.Abs(this.GetComponent<Rigidbody2D>().velocity.y) > 0)
        {
            animator.SetFloat("speed", m_MovementSpeed);
        }
        else
        {
            animator.SetFloat("speed", Mathf.Abs(this.GetComponent<Rigidbody2D>().velocity.x));
        }
        Vector2 delta = m_MovementAction.ReadValue<Vector2>();
        this.GetComponent<Rigidbody2D>().velocity = m_MovementSpeed * delta;
    }

    private void ActionPerformed(InputAction.CallbackContext context)
    {
        Debug.Log(context);
    }
}
