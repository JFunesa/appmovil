using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class ComidaPool : MonoBehaviour
{
    public static ComidaPool instance;
    private List<GameObject> pooledComida = new List<GameObject>();
    private int amountPool = 10;

    [SerializeField] private GameObject comida;

    private void Awake()
    {
        {
            if (instance == null)
            {
                if (instance == null)
                {
                    instance = this;
                }
            }
        }
    }

    void Start()
    {
        for (int i = 0; i < amountPool; i++) 
        {
            GameObject obj = Instantiate(comida);
            obj.SetActive(false);
            pooledComida.Add(obj);
        }
    }

    public GameObject GetPooledComida()
    {
        for (int i = 0; i < pooledComida.Count; i++)
        {
            if (!pooledComida[i].activeInHierarchy)
            {
                return pooledComida[i];
            }
        }
        return null;
    }
}
